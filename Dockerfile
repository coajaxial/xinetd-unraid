FROM alpine:3.19.0 as builder-xinted

RUN apk add --update --no-cache \
    alpine-sdk \
    autoconf \
    automake \
    xz

RUN addgroup -g 1000 www \
    && adduser -D -u 1000 -G www www \
    && install -d -o www -g www /opt/project /opt/project/src

USER www
WORKDIR /opt/project

RUN wget -O xinetd.tar.xz "https://github.com/openSUSE/xinetd/releases/download/2.3.15.4/xinetd-2.3.15.4.tar.xz" \
    && tar xf xinetd.tar.xz  --strip 1 --directory src \
    && cd src \
    && aclocal \
    && automake \
    && ./configure --prefix /opt/project \
    && make \
    && make install \
    && cd .. \
    && rm -rf src \
    && rm -f xinetd.tar.xz \
    && rm -rf etc/xinetd.d/*

FROM alpine:3.19.0

RUN apk add --update --no-cache \
    netcat-openbsd \
    docker-cli \
    supervisor \
    supercronic \
    moreutils \
    gettext \
    util-linux-misc

RUN addgroup -g 1000 www \
    && adduser -D -u 1000 -G www www \
    && install -d -o www -g www /opt/project

COPY --chown=root:root --link --from=builder-xinted /opt/project /
COPY xinetd.conf /etc/xinetd.conf
COPY xinetd.d /etc/xinetd.d
COPY start.sh stop.sh entrypoint.sh /bin/
COPY supervisord.conf /etc/supervisord.conf
COPY crontab /etc/crontab

ENV CONTAINER_NAME=""
ENV CONTAINER_PORT=""
ENV CONTAINER_PROTOCOL=""

CMD ["/bin/entrypoint.sh"]
