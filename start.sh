#!/bin/sh

set -e

docker start "$CONTAINER_NAME" > /dev/null 2>&1

while ! nc -z "$CONTAINER_NAME" "$CONTAINER_PORT" > /dev/null 2>&1; do
  sleep 1
done

nc "$CONTAINER_NAME" "$CONTAINER_PORT"
