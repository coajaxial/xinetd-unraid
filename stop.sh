#!/bin/sh

set -e

if [ "$( docker container inspect -f '{{.State.Status}}' "$CONTAINER_NAME" )" == "running" ]; then
  if ! pgrep -f '/bin/start\.sh'; then
      docker stop "$CONTAINER_NAME"
  fi
fi
