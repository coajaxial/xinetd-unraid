## Getting started

1. Enable `Preserve user defined networks` in `Settings > Docker` (You have to shut down array first, then restart it afterwards)
2. Open the Unraid Terminal and enter `docker create network <network-name>`, where `<network-name>` is a name of your choice.
3. Edit the container you want to enable on-demand for:
    - Remove the primary port binding, e.g. 25565 for minecraft
    - Change the `Network Type` from `bridge` to the newly created one in step 2
4. Add a new container:
   - Name: Choose one yourself
   - Repository: `registry.gitlab.com/coajaxial/xinetd-unraid/xinetd-unraid`
   - Network Type: Set to the newly created on in step 2
   - Add a new host path:
     - Host path: `/var/run/docker.sock`
     - Container path: `/var/run/docker.sock`
   - Add a new port:
     - Host port: Set to the port you removed from the container in step 3
     - Container port: Set to the port you removed from the container in step 3
   - Add a new variable:
     - Key: `CONTAINER_NAME`
     - Value: Set to the name of the container of step 3
   - Add a new variable:
     - Key: `CONTAINER_PROTOCOL`
     - Value: Set to `tcp` or `udp`, depending on the need
   - Add a new variable:
     - Key: `CONTAINER_PORT`
     - Value: Set to the port you removed from the container in step 3
   - ![A test image](docs/config.png)

## ToDo

- [x] Fix timing issues of start/stop
- [x] Add environment variables to configure host, port, etc.
- [x] Try to chase down "disconnected" errors on docker startup in minecraft