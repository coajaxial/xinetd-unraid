#!/bin/sh

set -e

FILE="/etc/xinetd.d/start"

envsubst '$CONTAINER_PORT,$CONTAINER_PROTOCOL' < "$FILE" | sponge "$FILE"

exec /usr/bin/supervisord -c /etc/supervisord.conf